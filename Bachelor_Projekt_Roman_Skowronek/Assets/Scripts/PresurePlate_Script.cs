﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresurePlate_Script : MonoBehaviour
{
    public bool platePressed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("PickUpAble"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
            platePressed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("PickUpAble"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);
            platePressed = false;
        }
    }
}
