﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] Animator door_Animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            door_Animator.SetBool("InDoorTrigger", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            door_Animator.SetBool("InDoorTrigger", false);
        }
    }
}
