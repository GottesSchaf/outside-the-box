﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [Range(1, 10)]
    [SerializeField] float jumpVelocity;
    [SerializeField] float exRay, backRay;
    bool grounded, prevJump;
    float playerHeight;
    [SerializeField] float delay;

    private void Awake()
    {
        playerHeight = GetComponent<CapsuleCollider>().height / 2 + exRay;
    }

    void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight + .1f);
        if (Input.GetButtonDown("Jump") && grounded)
        {
            GetComponent<Rigidbody>().velocity = Vector3.up * jumpVelocity;
        }
    }
}
