﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] GameObject loadingScreenObject, blackFadeObj;
    [SerializeField] Slider loadSlider;
    [SerializeField] Text progressText;
    [SerializeField] Animator fade_Animator;
    [SerializeField] string sceneName;

    IEnumerator ScreenFade()
    {
        fade_Animator.SetBool("InTrigger", true);
        yield return new WaitForSeconds(fade_Animator.GetCurrentAnimatorStateInfo(0).length);
        LoadLevel(sceneName);
    }

    public void LoadLevel(string sceneName)
    {
        StartCoroutine(LoadSceneAsynchronously(sceneName));
    }

    IEnumerator LoadSceneAsynchronously(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreenObject.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadSlider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }

    public void PlayGame()
    {
        blackFadeObj.SetActive(true);
        StartCoroutine(ScreenFade());
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
