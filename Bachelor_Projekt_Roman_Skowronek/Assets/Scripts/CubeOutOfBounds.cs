﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeOutOfBounds : MonoBehaviour
{
    [SerializeField] public GameObject cubePrefab;
    [SerializeField] Vector3 impulse = new Vector3(0, 0, 0);
    [SerializeField] public GameObject cube_SP;
    bool pushed = false;
    bool cubeSpawned;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PickUpAble")
        {
            Destroy(other.gameObject);
            Instantiate(cubePrefab, cube_SP.transform.position, Quaternion.identity);
            pushed = false;
        }
    }

    void FixedUpdate()
    {
        if(pushed == false)
        {
            //cubePrefab.GetComponent<Rigidbody>().velocity = impulse * 2;
            StartCoroutine(PushCube());
            //cubePrefab.GetComponent<Rigidbody>().AddForce(cubePrefab.transform.forward * 50f); //Schieße den Cube aus der Röhre heraus
            //pushed = true;
        }
    }

    IEnumerator PushCube()
    {
        for(int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.1f);
            //cubePrefab.GetComponent<Rigidbody>().AddForce(impulse, ForceMode.VelocityChange); //Schieße den Cube aus der Röhre heraus
            cubePrefab.GetComponent<Rigidbody>().velocity = impulse * 2;
        }
        pushed = true;
    }
}
