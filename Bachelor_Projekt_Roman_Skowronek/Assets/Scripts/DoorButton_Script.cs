﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorButton_Script : MonoBehaviour
{
    public bool buttonPressed;
    bool corCheck;

    private void OnMouseDown()
    {
        if (!corCheck)
        {
            if (!buttonPressed)
            {
                buttonPressed = true;
                StartCoroutine(BtnPressedCor());
            }
            else
            {
                buttonPressed = false;
                StartCoroutine(BtnPressedCor());
            }
        }
    }

    IEnumerator BtnPressedCor()
    {
        corCheck = true;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.05f, this.transform.position.z);
        yield return new WaitForSeconds(0.5f);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.05f, this.transform.position.z);
        corCheck = false;
    }
}
