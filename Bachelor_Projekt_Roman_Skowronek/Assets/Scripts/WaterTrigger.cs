﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTrigger : MonoBehaviour
{
    GameObject playerCP;
    [SerializeField] GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player.transform.position = playerCP.transform.position;
            player.transform.rotation = playerCP.transform.rotation;
        }
    }

    public GameObject m_playerCP
    {
        get { return playerCP; }
        set { playerCP = value; Debug.Log("Set the CP!"); }
    }
}
