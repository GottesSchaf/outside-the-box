﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorScript : MonoBehaviour
{

    [SerializeField] float endY;
    [SerializeField] GameObject ceiling, floor, button, player;
    [SerializeField] Transform floorTrans;
    [SerializeField] float speedMultiplier = 1f;
    [SerializeField] Collider otherTrigger;
    bool buttonPressed, goingDown, goingUp, finished, playerInElevator, btnPressed;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (buttonPressed)
        {
            if (floor.transform.position.y > endY && goingDown)
            {
                floor.transform.position -= floor.transform.up * Time.deltaTime * speedMultiplier;
                ceiling.transform.position -= ceiling.transform.up * Time.deltaTime * speedMultiplier;
                button.transform.position -= button.transform.up * Time.deltaTime * speedMultiplier;
            }
            else if (floor.transform.position.y < endY && goingUp)
            {
                floor.transform.position += floor.transform.up * Time.deltaTime * speedMultiplier;
                ceiling.transform.position += ceiling.transform.up * Time.deltaTime * speedMultiplier;
                button.transform.position += button.transform.up * Time.deltaTime * speedMultiplier;
            }
            else
            {
                buttonPressed = false;
                if(floor.transform.position.y <= endY && !finished)
                {
                    floor.transform.position = new Vector3(floor.transform.position.x, endY, floor.transform.position.z);
                    ceiling.transform.position = new Vector3(ceiling.transform.position.x, endY + 4f, ceiling.transform.position.z);
                    button.transform.position = new Vector3(button.transform.position.x, endY + 1.5f, button.transform.position.z);
                    finished = true;
                    player.transform.SetParent(null);
                }
                else if (floor.transform.position.y >= endY && !finished)
                {
                    floor.transform.position = new Vector3(floor.transform.position.x, endY, floor.transform.position.z);
                    ceiling.transform.position = new Vector3(ceiling.transform.position.x, endY + 4f, ceiling.transform.position.z);
                    button.transform.position = new Vector3(button.transform.position.x, endY + 1.5f, button.transform.position.z);
                    finished = true;
                    player.transform.SetParent(null);
                }
            }
        }
    }

    private void OnMouseDown()
    {
        if (playerInElevator)
        {
            buttonPressed = true;
            if (!btnPressed)
            {
                StartCoroutine(BtnPushed());
            }
            if (floor.transform.position.y > endY)
            {
                goingDown = true;
                player.transform.SetParent(floorTrans);
            }
            else if (floor.transform.position.y < endY)
            {
                goingUp = true;
                player.transform.SetParent(floorTrans);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            playerInElevator = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            playerInElevator = false;
        }
    }

    IEnumerator BtnPushed()
    {
        btnPressed = true;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 0.025f);
        yield return new WaitForSeconds(0.5f);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 0.025f);
        btnPressed = false;
    }
}
