﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    float timer, waitForSec = 15f;

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer >= waitForSec)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
