﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor_Room7 : MonoBehaviour
{
    [SerializeField] GameObject door;
    [SerializeField] Animator door_Animator;
    [SerializeField] PresurePlate_Script presPlate;
    [SerializeField] DoorButton_Script doorBtn;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && presPlate.platePressed && doorBtn.buttonPressed)
        {
            door_Animator.SetBool("InDoorTrigger", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            door_Animator.SetBool("InDoorTrigger", false);
        }
    }
}
