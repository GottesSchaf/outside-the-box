﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawn_Button : MonoBehaviour
{
    [SerializeField] CubeSpawn cubeSpawn;
    bool btnPressed;

    private void OnMouseDown()
    {
        if (!btnPressed)
        {
            cubeSpawn.spawnNewCube = true;
            StartCoroutine(BtnPushed());
        }
    }

    IEnumerator BtnPushed()
    {
        btnPressed = true;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.05f, this.transform.position.z);
        yield return new WaitForSeconds(0.5f);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.05f, this.transform.position.z);
        btnPressed = false;
    }
}
