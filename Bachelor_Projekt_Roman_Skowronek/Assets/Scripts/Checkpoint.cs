﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject checkpoint;
    [SerializeField] WaterTrigger waterTr, puzzleTr2;
    Material[] mats;
    [SerializeField] Material matGreen;
    bool visitedCP;

    private void OnTriggerEnter(Collider other)
    {
        //Wenn der Spieler in den Trigger kommt, setze ihm einen Checkpoint und zeige es durch den Bildschirm
        if (other.CompareTag("Player") && !visitedCP)
        {
            waterTr.m_playerCP = checkpoint;
            puzzleTr2.m_playerCP = checkpoint;
            mats = this.GetComponent<Renderer>().materials;
            mats[2] = matGreen;
            this.GetComponent<Renderer>().materials = mats;
            visitedCP = true;
        }
    }
}
