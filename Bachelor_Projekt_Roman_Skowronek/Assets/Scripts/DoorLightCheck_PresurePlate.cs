﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLightCheck_PresurePlate : MonoBehaviour
{
    [SerializeField] PresurePlate_Script presPlate;
    [SerializeField] Material matGreen, matRed;

    private void FixedUpdate()
    {
        if (presPlate.platePressed)
        {
            GetComponent<Renderer>().material = matGreen;
        }
        else if(!presPlate.platePressed)
        {
            GetComponent<Renderer>().material = matRed;
        }
    }
}
