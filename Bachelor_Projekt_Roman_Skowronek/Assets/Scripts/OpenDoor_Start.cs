﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenDoor_Start : MonoBehaviour
{
    [SerializeField] GameObject door, loadingScreenObject;
    [SerializeField] Slider loadSlider;
    [SerializeField] Text progressText;
    [SerializeField] Animator door_Animator, fade_Animator;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            door_Animator.SetBool("InDoorTrigger", true);
            StartCoroutine(ScreenFade());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            door_Animator.SetBool("InDoorTrigger", false);
        }
    }

    IEnumerator ScreenFade()
    {
        fade_Animator.SetBool("InTrigger", true);
        yield return new WaitForSeconds(fade_Animator.GetCurrentAnimatorStateInfo(0).length); //+ fade_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime
        LoadLevel("Game");
    }

    public void LoadLevel(string sceneName)
    {
        StartCoroutine(LoadSceneAsynchronously(sceneName));
    }

    IEnumerator LoadSceneAsynchronously(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreenObject.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadSlider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }
}
