﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawn_Button_Room7 : MonoBehaviour
{
    [SerializeField] CubeSpawn cubeSpawn;
    bool btnPressed;
    [SerializeField] PresurePlate_Script presPlate;

    private void OnMouseDown()
    {
        if (!btnPressed)
        {
            presPlate.platePressed = false;
            cubeSpawn.spawnNewCube = true;
            StartCoroutine(BtnPushed());
        }
    }

    IEnumerator BtnPushed()
    {
        btnPressed = true;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.05f, this.transform.position.z);
        yield return new WaitForSeconds(0.5f);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.05f, this.transform.position.z);
        btnPressed = false;
    }
}
