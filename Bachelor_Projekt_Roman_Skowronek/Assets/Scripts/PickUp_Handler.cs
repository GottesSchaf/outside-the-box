﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Handler : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject carriedObj;
    bool carrying;
    [SerializeField] float distance;
    [SerializeField] float smooth;
    [SerializeField] float maxRaycastDist;

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if (carrying)
        {
            Carry(carriedObj);
            CheckDrop();
        }
        else
        {
            PickUp();
        }
    }

    void Carry(GameObject Obj)
    {
        Obj.transform.position = Vector3.Lerp(Obj.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
        Obj.transform.rotation = Quaternion.identity;
    }

    void PickUp()
    {
        if (Input.GetMouseButtonDown(0))
        {
            int x = Screen.width / 2;
            int y = Screen.height / 2;

            Ray ray = mainCamera.ScreenPointToRay(new Vector3(x, y));
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, maxRaycastDist))
            {
                if(hit.transform.tag == "PickUpAble")
                {
                    hit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
                    carrying = true;
                    carriedObj = hit.transform.gameObject;
                }
                else
                {
                    Debug.Log("Not able to pickup that Object!");
                }
            }
        }
    }

    void CheckDrop()
    {
        if (Input.GetMouseButtonDown(1))
        {
            DropObj();
        }
    }

    void DropObj()
    {
        if (carriedObj != null)
        {
            carriedObj.GetComponent<Rigidbody>().useGravity = true;
            carriedObj = null;
            carrying = false;
        }
        else
        {
            Debug.Log("carriedObj is Null!!!");
        }
    }
}
