﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTriggerEnabler : MonoBehaviour
{
    [SerializeField] GameObject cubeTrigger, cubeTrigger2;
    bool isActive = true, corCheck;

    private void OnMouseDown()
    {
        if (!corCheck)
        {
            StartCoroutine(BtnPressedCor());
            if (isActive)
            {
                isActive = false;
                cubeTrigger.SetActive(false);
                cubeTrigger2.SetActive(false);
            }
            else if (!isActive)
            {
                isActive = true;
                cubeTrigger.SetActive(true);
                cubeTrigger2.SetActive(true);
            }
        }
    }

    IEnumerator BtnPressedCor()
    {
        corCheck = true;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.05f, this.transform.position.z);
        yield return new WaitForSeconds(0.5f);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.05f, this.transform.position.z);
        corCheck = false;
    }
}
