﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLightButton : MonoBehaviour
{
    [SerializeField] DoorButton_Script doorBtn;
    [SerializeField] Material matGreen, matRed;

    private void FixedUpdate()
    {
        if (doorBtn.buttonPressed)
        {
            GetComponent<Renderer>().material = matGreen;
        }
        else if (!doorBtn.buttonPressed)
        {
            GetComponent<Renderer>().material = matRed;
        }
    }
}
