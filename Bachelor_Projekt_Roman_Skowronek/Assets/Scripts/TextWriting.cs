﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWriting : MonoBehaviour
{
    [SerializeField] string dialogue;
    [SerializeField] Text textAusgabe;
    [SerializeField] GameObject textFenster, textFieldPressF;
    bool visited, textWritten, stopWriting;
    [SerializeField] float waitForSec;
    int thisObjInt, checkInt;

    private void Start()
    {
        thisObjInt = int.Parse(gameObject.name.Substring(0, 1));
    }

    private void Update()
    {
        //Wenn Text Aufbau vorzeitig abgebrochen, dann fülle die Textbox sofort komplett aus
        if (Input.GetKeyDown(KeyCode.F) && !textWritten && thisObjInt == checkInt)
        {
            stopWriting = true;
            textAusgabe.text = "";
            textAusgabe.text = dialogue;
            textWritten = true;
        }
        //Wenn Text fertig geschrieben wurde, schließe das Dialog Fenster
        else if(Input.GetKeyDown(KeyCode.F) && textWritten && thisObjInt == checkInt)
        {
            textFieldPressF.SetActive(false);
            textAusgabe.text = "";
            textFenster.SetActive(false);
            StopAllCoroutines();
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        checkInt = int.Parse(gameObject.name.Substring(0, 1));
        if (other.CompareTag("Player") && !visited)
        {
            textAusgabe.text = "";
            visited = true;
            textWritten = false;
            stopWriting = false;
            textFenster.transform.localScale = new Vector3(1, 0, 1);
            Coroutine animBox = StartCoroutine(AnimateTextbox());
        }
    }
    
    IEnumerator AnimateText(string strComplete)
    {
        int i = 0;
        textAusgabe.text = "";
        while (i < strComplete.Length)
        {
            if (stopWriting)
            {
                break;
            }
            else
            {
                textAusgabe.text += strComplete[i++];
                yield return new WaitForSeconds(waitForSec);
            }
        }
        if(i == strComplete.Length)
        {
            textWritten = true;
        }
    }

    IEnumerator AnimateTextbox()
    {
        textFenster.SetActive(true);
        for (float i = 0; i < 1; i += 0.1f)
        {
            textFenster.transform.localScale = new Vector3(1, i, 1);
            yield return new WaitForSeconds(0.001f);
        }
        textFieldPressF.SetActive(true);
        dialogue = dialogue.Replace("\\n", "\n");
        Coroutine animText = StartCoroutine(AnimateText(dialogue));
    }
}
