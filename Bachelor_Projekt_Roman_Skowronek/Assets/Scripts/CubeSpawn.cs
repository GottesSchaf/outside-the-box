﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawn : MonoBehaviour
{
    [SerializeField] CubeOutOfBounds cubeSpawn;
    public bool spawnNewCube;

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "PickUpAble" && spawnNewCube)
        {
            Destroy(other.gameObject);
            Instantiate(cubeSpawn.cubePrefab, cubeSpawn.cube_SP.transform.position, Quaternion.identity);
            spawnNewCube = false;
        }
    }
}
