﻿/* 
 * author : jiankaiwang
 * description : The script provides you with basic operations of first personal control.
 * platform : Unity
 * date : 2017/12
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float speed = 2f;
    float speedSafe;
    private float translation;
    private float strafe;
    float sprintSpeed = 5f;
    public float jumpHeight = 5f;
    public Rigidbody playerCapsule;

    void Start()
    {
        speedSafe = speed; //Um Speed Value zu speichern
        Cursor.lockState = CursorLockMode.Locked; //turn off the cursor
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = sprintSpeed;
        }
        else
        {
            speed = speedSafe;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
        }
        // You can further set it on Unity. (Edit > Project Settings > Input)
        translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        strafe = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        transform.Translate(strafe, 0, translation);
        

        if (Input.GetKeyDown("escape"))
        {
            // turn on the cursor
            Cursor.lockState = CursorLockMode.None;
        }
    }
}