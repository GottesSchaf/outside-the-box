﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsWriter : MonoBehaviour
{
    [SerializeField] string tip;
    [SerializeField] Text textAusgabe;
    [SerializeField] GameObject textFenster, textFieldPressF;
    bool visited, textWritten, stopWriting;
    [SerializeField] float waitForSec;
    float timer;
    [SerializeField] float timeToTip;
    int thisObjInt, checkInt;

    private void Start()
    {
        thisObjInt = int.Parse(gameObject.name.Substring(0, 2));
    }
    private void Update()
    {
        if (timer >= timeToTip)
        {
            //Wenn Text Aufbau vorzeitig abgebrochen, dann fülle die Textbox sofort komplett aus
            if (Input.GetKeyDown(KeyCode.F) && !textWritten && thisObjInt == checkInt)
            {
                stopWriting = true;
                textAusgabe.text = "";
                textAusgabe.text = tip;
            }
            //Wenn Text fertig geschrieben wurde, schließe das Dialog Fenster
            else if (Input.GetKeyDown(KeyCode.F) && textWritten && thisObjInt == checkInt)
            {
                textFieldPressF.SetActive(false);
                textAusgabe.text = "";
                textFenster.SetActive(false);
                StopAllCoroutines();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            timer = 0;
            checkInt = int.Parse(gameObject.name.Substring(0, 2));
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !visited)
        {
            timer += Time.deltaTime;
            if(timer >= timeToTip)
            {
                textAusgabe.text = "";
                visited = true;
                textWritten = false;
                stopWriting = false;
                textFenster.transform.localScale = new Vector3(1, 0, 1);
                Coroutine animBox = StartCoroutine(AnimateTextbox());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            timer = 0;
            visited = false;
        }
    }

    IEnumerator AnimateText(string strComplete)
    {
        int i = 0;
        textAusgabe.text = "";
        while (i < strComplete.Length)
        {
            if (stopWriting)
            {
                break;
            }
            else
            {
                textAusgabe.text += strComplete[i++];
                yield return new WaitForSeconds(waitForSec);
            }
        }
        textWritten = true;
    }

    IEnumerator AnimateTextbox()
    {
        textFenster.SetActive(true);
        for (float i = 0; i < 1; i += 0.1f)
        {
            textFenster.transform.localScale = new Vector3(1, i, 1);
            yield return new WaitForSeconds(0.001f);
        }
        textFieldPressF.SetActive(true);
        tip = tip.Replace("\\n", "\n");
        Coroutine animText = StartCoroutine(AnimateText(tip));
    }
}
